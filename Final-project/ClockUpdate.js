Module.register("MMM-currentTime", {
  defaults: {},
  start: function (){
    this.count = 0
    var timer = setInterval(()=>{
      this.updateDom()
      this.count++
    }, 30000)
  },
  getDom: function() {
    var element = document.createElement("div")
    element.className = "currentTime"
    element.innerHTML = moment().format('LTS')
    /*
    var subElement = document.createElement("p")
    subElement.innerHTML = "Count:" + this.count
    subElement.id = "COUNT"
    element.appendChild(subElement)
    */
    return element
  },
  notificationReceived: function() {},
  socketNotificationReceived: function() {},
}) 


