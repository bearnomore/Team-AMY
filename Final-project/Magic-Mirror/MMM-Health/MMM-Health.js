/* Smart Mirror - Apple Health Tracker Config 
 *
 * By Team AMY 
 * ankura2@illinois.edu
 * markf4@illinois.edu
 * yex2@illinois.edu
 * 
 * 
 * For more information on how you can configure this file
 * see https://docs.magicmirror.builders/getting-started/configuration.html#general
 * and https://docs.magicmirror.builders/modules/configuration.html
 */
const https = require('https')
Module.register("MMM-Health", {
	// defining the JSON that will be send back from salesforce server
	jsonData : null,
	// Default module config.
	defaults: {
		updateInterval : 1000,
		metric : 'walking_running_distance',
		metricLabel : 'Steps',
		userEmail : 'test@test.com'
	},

	getTemplateData: function () {
		return this.config;
	},
	start: function () {
		//Making call to get Health JSON
		this.getJSON();
		//Update the screen with the JSON data
		this.updateDom();
		
		var self = this;
		setInterval(function(){
			self.getJSON();
		},this.config.updateInterval);
		
	},
	// Override dom generator.
	getJSON: function () {
		var self = this;
		console.log(this.config.metric);
		//Defining URL and passing parameters to Salesforce to get health data
		var options = {
			hostname : 'magicmirrorhealth-developer-edition.na163.force.com',
			path :'/services/apexrest/getHealthData?email='+ this.config.userEmail + '&metric=' + this.config.metric
		}
		https.get(options, function(res) {
		  console.log("Got response: " + res.statusCode);

		  res.on("data", function(chunk) {
			
		    console.log("BODY: " + chunk);

		    self.jsonData = chunk;

		    self.updateDom();
		  });
		}).on('error', function(e) {
		  console.log("Got error: " + e.message);
		});
	},
	//This method update the Screen with the data
	getDom: function () {

		var wrapper = document.createElement("div");
		if(!this.jsonData){
			wrapper.innerHTML = 'Awaiting JSON....';
		}
		else{
 			var i = this.jsonData;

			var j = 0;

			//Create the header of the table
			var x = document.createElement("TABLE");
			x.setAttribute("id", "myTable");
			var y = document.createElement("TR");
			y.setAttribute("id", "myTr" + j);
			x.appendChild(y);
			var z = document.createElement("TD");
			z.style.padding = "0px 40px";
			z.innerHTML = 'Date';
			y.appendChild(z);
			z = document.createElement("TD");
			z.innerHTML = this.config.metricLabel;
			y.appendChild(z);

			var abc = '';
			for(temp of JSON.parse(this.jsonData)){
			abc = abc + temp
			}
			console.log(JSON.parse(abc).length)
			for(var temp of JSON.parse(abc)){ 

				j = j + 1;
				y = document.createElement("TR");
				y.setAttribute("id", "myTr" + j);
				x.appendChild(y);

				z = document.createElement("TD");
				z.style.padding = "0px 40px";
				z.innerHTML = temp.displayText.split(' ')[0];
			
				y.appendChild(z);
				z = document.createElement("TD");
				
				z.innerHTML = Math.round(parseFloat(temp.steps) * (10 ^ 2)) / (10 ^ 2);
				y.appendChild(z);
			}
			wrapper = x;
		}

		return wrapper;
	}
});
