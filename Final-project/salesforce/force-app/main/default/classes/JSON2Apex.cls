

public class JSON2Apex {

    public class Metrics {
        public String name {get;set;} 
        public String units {get;set;} 
        public List<Data> data {get;set;} 
        public List<Data_Y> data_y {get;set;} 

        public Metrics(JSONParser parser) {
            while (parser.nextToken() != System.JSONToken.END_OBJECT) {
                if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
                    String text = parser.getText();
                    if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
                        if (text == 'name') {
                            name = parser.getText();
                        } else if (text == 'units') {
                            units = parser.getText();
                        } else if (text == 'data') {
                            //data = arrayOfData(parser);
                            data_y = arrayOfData_Y(parser);
                        } else {
                            System.debug(LoggingLevel.WARN, 'Metrics consuming unrecognized property: '+text);
                            consumeObject(parser);
                        }
                    }
                }
            }
        }
    }
    
    public class Data_Y {
        public Double qty {get;set;} 
        public String date_Z {get;set;} // in json: date

        public Data_Y(JSONParser parser) {
            while (parser.nextToken() != System.JSONToken.END_OBJECT) {
                if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
                    String text = parser.getText();
                    if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
                        if (text == 'qty') {
                            qty = parser.getDoubleValue();
                        } else if (text == 'date') {
                            date_Z = parser.getText();
                        } else {
                            System.debug(LoggingLevel.WARN, 'Data_Y consuming unrecognized property: '+text);
                            consumeObject(parser);
                        }
                    }
                }
            }
        }
    }
    
    public class Data_Z {
        public List<Metrics> metrics {get;set;} 
        public List<Data> workouts {get;set;} 

        public Data_Z(JSONParser parser) {
            while (parser.nextToken() != System.JSONToken.END_OBJECT) {
                if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
                    String text = parser.getText();
                    if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
                        if (text == 'metrics') {
                            metrics = arrayOfMetrics(parser);
                        } else if (text == 'workouts') {
                            workouts = arrayOfData(parser);
                        } else {
                            System.debug(LoggingLevel.WARN, 'Data_Z consuming unrecognized property: '+text);
                            consumeObject(parser);
                        }
                    }
                }
            }
        }
    }
    
    public Data_Z data {get;set;} 

    public JSON2Apex(JSONParser parser) {
        while (parser.nextToken() != System.JSONToken.END_OBJECT) {
            if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
                String text = parser.getText();
                if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
                    if (text == 'data') {
                        data = new Data_Z(parser);
                    } else {
                        System.debug(LoggingLevel.WARN, 'JSON2Apex consuming unrecognized property: '+text);
                        consumeObject(parser);
                    }
                }
            }
        }
    }
    
    public class Data {

        public Data(JSONParser parser) {
            while (parser.nextToken() != System.JSONToken.END_OBJECT) {
                if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
                    String text = parser.getText();
                    if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
                        {
                            System.debug(LoggingLevel.WARN, 'Data consuming unrecognized property: '+text);
                            consumeObject(parser);
                        }
                    }
                }
            }
        }
    }
    
    
    public static JSON2Apex parse(String json) {
        System.JSONParser parser = System.JSON.createParser(json);
        return new JSON2Apex(parser);
    }
    
    public static void consumeObject(System.JSONParser parser) {
        Integer depth = 0;
        do {
            System.JSONToken curr = parser.getCurrentToken();
            if (curr == System.JSONToken.START_OBJECT || 
                curr == System.JSONToken.START_ARRAY) {
                depth++;
            } else if (curr == System.JSONToken.END_OBJECT ||
                curr == System.JSONToken.END_ARRAY) {
                depth--;
            }
        } while (depth > 0 && parser.nextToken() != null);
    }
    



    private static List<Data_Y> arrayOfData_Y(System.JSONParser p) {

        List<Data_Y> res = new List<Data_Y>();
        if (p.getCurrentToken() == null) p.nextToken();
        while (p.nextToken() != System.JSONToken.END_ARRAY) {
            res.add(new Data_Y(p));
        }
        return res;
    }


    private static List<Metrics> arrayOfMetrics(System.JSONParser p) {
        List<Metrics> res = new List<Metrics>();
        if (p.getCurrentToken() == null) p.nextToken();
        while (p.nextToken() != System.JSONToken.END_ARRAY) {
            res.add(new Metrics(p));
        }
        return res;
    }








    private static List<Data_Z> arrayOfData_Z(System.JSONParser p) {

        List<Data_Z> res = new List<Data_Z>();
        if (p.getCurrentToken() == null) p.nextToken();
        while (p.nextToken() != System.JSONToken.END_ARRAY) {
            res.add(new Data_Z(p));
        }
        return res;
    }


    private static List<Data> arrayOfData(System.JSONParser p) {

        List<Data> res = new List<Data>();
        if (p.getCurrentToken() == null) p.nextToken();
        while (p.nextToken() != System.JSONToken.END_ARRAY) {

            res.add(new Data(p));
        }
        return res;
    }




}