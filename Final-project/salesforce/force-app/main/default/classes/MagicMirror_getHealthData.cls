@RestResource(urlMapping='/getHealthData')
global class MagicMirror_getHealthData {
    @HttpGet
    global static string doGet() {
        //String name = RestContext.request;
        //requestBody
        String requestedMetric = RestContext.request.params.get('metric');
        String email = RestContext.request.params.get('email');
        list<Activity__c > lstActivity = [select 
                                            id, Unit__c,Metric_Type__c,Quantity__c,Date__c,Date_string__c
                                         from Activity__c 
                                         where contact__r.email = :email and date__C =  LAST_N_DAYS:7 and metric_type__c = :requestedMetric ];
        
        list<MagicMirror_wrapper> lstMM_Wrapper = new list<MagicMirror_wrapper>();
        
        for(Activity__c tempActivity : lstActivity ){
            MagicMirror_wrapper tempWrapper =  new MagicMirror_wrapper(string.valueof(tempActivity.Quantity__c),tempActivity.Date_string__c  );
            lstMM_Wrapper .add(tempWrapper );
        }
        return JSON.serialize(lstMM_Wrapper );
        //return json.serialize(system.now());
        //return (string)acc;
    }
}