@RestResource(urlMapping='/postHealth')
global class MagicMirror_postHealth{
    @HttpPost
    global static string doGet() {

        set<String> capture_Metrics = new set<string>();
        
        
        capture_Metrics.add('flights_climbed');
        capture_Metrics.add('step_count');
        capture_Metrics.add('walking_running_distance');
        capture_Metrics.add('walking_asymmetry_percentage');
        capture_Metrics.add('walking_double_support_percentage');
        capture_Metrics.add('walking_speed');
        capture_Metrics.add('walking_step_length');
        system.debug(RestContext.request.params);

        JSON2Apex jsData = JSON2Apex.parse(RestContext.request.requestBody.tostring());
        //system.debug(jsDAta.data.metrics);
        contact cont = new contact();
        list<contact> lstcont = [select id from contact where email =: RestContext.request.params.get('email')];
        system.debug(lstcont);
        if(lstcont.size() == 0 ){
            //contact cont = new contact();
            cont.firstname = RestContext.request.params.get('firstname');
            cont.lastname = RestContext.request.params.get('lastname');
            cont.email = RestContext.request.params.get('email');
            insert cont;
        }
        else{
            cont = lstcont[0];
        }
        list<Activity__c> lstActivity = new list<activity__c>();
        for(JSON2APEX.Metrics metric : jsDAta.data.metrics){
            if(capture_Metrics.contains(metric.name)){
                for(JSON2APEX.data_y data : metric.data_y ){
                    Activity__c newACtivity = new Activity__c();
                    newACtivity.Metric_Type__c = metric.name;
                    newACtivity.Unit__c = metric.units;
                    newACtivity.Date_String__c = data.date_Z;
                    
                    string abc = newACtivity.Date_String__c.split(' ')[0];
                    list<string> datestr = abc.split('-');
                    //date.newInstance(2021, 11, 12);
                    newACtivity.Date__c = date.newInstance(integer.valueOf(datestr[0]), integer.valueOf(datestr[1]), integer.valueOf(datestr[2]));
                    
                    newACtivity.Quantity__c  = data.qty;
                    newACtivity.Contact__c = cont.id;
                    newactivity.external_Id__c = newACtivity.Contact__c + newACtivity.Date_String__c.split(' ')[0] + newACtivity.Metric_Type__c;
                    lstActivity.add(newACtivity );
                    //system.debug(newActivity);
                }
                //system.debug(metric);
                //system.debug(metric.data);
            }                
        }
        system.debug(lstActivity .size());
        Schema.SObjectField f = Activity__c.Fields.external_Id__c ;
        database.upsert(lstActivity, f);
        //upsert lstActivity ;
         //system.debug(JSON2Apex.parse(RestContext.request.requestBody.tostring()));
        return 'SUCCESS';
    }
}