public class MagicMirror_wrapper{
    public string steps;
    public string displayText;
    
    public MagicMirror_wrapper(){
        this.steps = '';
        this.displayText = ''; 
    }
    public MagicMirror_wrapper(string steps , string displayText){
        this.steps = steps;
        this.displayText = displayText; 
    }
}