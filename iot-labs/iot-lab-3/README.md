This directory is for CS437 IoT Lab3 part3.

"wifi_signal_check.py" is to extract the "link_quality parameter" of the output from the command "iwconfig wlan0" using Python library "subprocess".

"keyboard_control.py" and "bt_server_wifiqual.py" are scripts directing the Pi car using either keyboard or bluetooth to move around in the room to check the WiFi signal. The above "wifi_signal_check.py" is implemented as the function "wifi_quality()". 

"Commands_used_in_Lab3.txt" is the list of command lines used for Step1 adn Step3 for firewall setup (through ufw) and Wireless configuration.
