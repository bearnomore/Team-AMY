import bluetooth
import picar_4wd as fc
import json
import time
import subprocess

SPEED = 25

hostMACAddress = "E4:5F:01:40:57:A3" # The address of Raspberry PI Bluetooth adapter on the server. The server might have multiple Bluetooth adapters.
port = 0
backlog = 1
size = 1024
s = bluetooth.BluetoothSocket(bluetooth.RFCOMM)
s.bind((hostMACAddress, port))
s.listen(backlog)
print("listening on port ", port)

def wifi_quality():
    cmd = 'iwconfig'

    out = subprocess.Popen([cmd, 'wlan0'], stdout = subprocess.PIPE)

    output = str(out.communicate())
    wifi_qual_ind = output.find("Link Quality=")
    wifi_qual = output[wifi_qual_ind: wifi_qual_ind+18]

    return wifi_qual

try:
    client, clientInfo = s.accept()
    while 1:
        print("server recv from: ", clientInfo)
        data = client.recv(size)
        print('Receive direction from the client: ', data)
        if data != b"":
            direction = data.decode().lower()
            print('The direction Pi car received from the client: ', direction)

            if direction == "forward" or direction == "f":
                fc.forward(SPEED)
                distance = 0
                for i in range(5):
                    time.sleep(0.1)
                    distance += SPEED * 0.1
            if direction == "backward" or direction == "b":
                fc.backward(SPEED)
                distance = 0
                for i in range(5):
                    time.sleep(0.1)
                    distance += SPEED * 0.1

            if direction == "left" or direction == "l":
                fc.turn_left(50)
                for i in range(10):
                    time.sleep(0.1)
                fc.forward(SPEED)
                distance = 0
                for i in range(5):
                    time.sleep(0.1)
                    distance += SPEED * 0.1

            if direction == "right" or direction == "r":
                fc.turn_right(50)
                for i in range(10):
                    time.sleep(0.1)

                fc.forward(SPEED)
                distance = 0
                for i in range(5):
                    time.sleep(0.1)
                    distance += SPEED * 0.1

            fc.stop()
			wifi_qual = wifi_quality()
			print(wifi_qual)

            # Read stats of pi car and add the distance and speed to the parameter list
            stats = fc.pi_read()
            stats["Direction"] = direction
            stats["Travel_Distance"] = distance
            stats["Car_Speed"] = SPEED
			stats["WiFi_quality"] = wifi_qual
            print('Pi car stats after executing the direction from the client: ', stats)

            stats = json.dumps(stats)
            client.sendall(stats.encode()) # Echo back to client

#if data:
        #    print(data)
        #    client.send(data) # Echo back to client
except:
    print("Closing socket")
    client.close()
    s.close()
