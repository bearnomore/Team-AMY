/*
Connection of GPS to Arduino followed the lab instruction.
Coonection of Ultrasonic Sensro: 
   VCC pin (left most) of Ultrasonic Sensor linked to 5V pin of Arduino;
   TRig pin (second left) of Ultrasonic Sensor linked to Digital IO pin7 of Arduino;
   Echo pin (third left) of Ultrasonic Sensor linked to Digital IO pin6 of Arduino;
   GND pin (right most) of Ultrasonic Sensor linked to GND pin of Arduino.
*/

Adafruit_GPS GPS;
const int pingPin = 7; // Trigger Pin of Ultrasonic Sensor
const int echoPin = 6; // Echo Pin of Ultrasonic Sensor

long microsecondsToInches(long microseconds) {
   return microseconds*0.0135/2;
}

long microsecondsToCentimeters(long microseconds) {
   return microseconds*0.0343/2;
}

void setup() { 
	// put your setup code here, to run once:
   pinMode(0, OUTPUT); //For net working
	pinMode(2, OUTPUT); //For GPS

   pinMode(pingPin, OUTPUT); // For Ultra
   pinMode(echoPin, INPUT); // For Ultra

	Serial.begin(9600);
}

void loop() { 
	// put your main code here, to run repeatedly:
   // GPS
	if(Serial.available() > 0){
		string gpsResult = GPS.read();
		Serial.println(gpsResult);
	}else{Serial.write("No data");}
   Serial.println("");
   
   //Ultrasonic
   long duration, inches, cm;
   digitalWrite(pingPin, LOW);
   delayMicroseconds(2);
   digitalWrite(pingPin, HIGH);
   delayMicroseconds(10);
   digitalWrite(pingPin, LOW);
   duration = pulseIn(echoPin, HIGH);
   inches = microsecondsToInches(duration);
   cm = microsecondsToCentimeters(duration);
   Serial.print(inches);
   Serial.print("in, ");
   Serial.print(cm);
   Serial.print("cm");
   Serial.println("");
   
   string msg = "Hello \n";
   Serial.sendMessage(0, msg);

	delay(100);
}


