/*
Connection of Vital sensor to Arduino followed the lab instruction.
Connection of temperature sensor to Arduino:
   VCC pin (left) connected to 5V pin of Arduino;
   OUT pin (center) connected to Analog Input pin A0 (Digital IO Pin 14) of Arduino;
   GND pin (righ) connected to GND pin of Arduino;
*/

PulseOximeter pox;
float temp;
int tempPin = 0;

void setup() {
    pinMode(0, OUTPUT);
    pinMode(2, OUTPUT);
    pinMode(3, OUTPUT);
    Serial.begin(9600);  
}

void loop() {
   if (Serial.available() > 0) {
      string mystr = Serial.readMessage();
      Serial.println("\nPrinted by receiver: " + mystr);

      if (pox.begin()) {
         string spO2_res = pox.getSpO2();
         string hb_res = pox.getHeartRate();
         Serial.write("this is oxygen percentage: " + spO2_res + ". this is heart rate: " + hb_res);
      }
      digitalWrite(3, HIGH);
   }
   else{Serial.write("No data \n");
        digitalWrite(3, LOW);
   }

   temp = analogRead(tempPin);
   // read analog volt from sensor and save to variable temp
   temp = temp * 0.48828125;
   // convert the analog volt to its temperature equivalent
   Serial.print("TEMPERATURE = ");
   Serial.print(temp); // display temperature value
   Serial.print("*C");
   Serial.println("");

   delay(1000); // update sensor reading each one second
      
}